import mne
import matplotlib.pyplot as plt

from data_prep import get_data_iterator


# === MAIN ===

def main():
    # Here is some minimal example of how to work with the preprocessed data :-)
    data_iter = get_data_iterator("inferred/epochs", select="*-auto-epo.fif")
    epochs_h01 = next(x for x in data_iter)
    epochs_h01.plot(n_epochs=10)
    plt.show()


if __name__ == '__main__':
    mne.set_log_level(verbose=False)
    main()
