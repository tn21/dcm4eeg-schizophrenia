% analyse some ERP data (mismatch negativity ERP SPM file from SPM-webpages)
% This is an example batch script to analyse two evoked responses with an
% assumed 5 sources.
% To try this out on your data (the date of this example don't exist in your SPM8 distribution), 
% you have to change 'Pbase' to your own analysis-directory, and choose a name ('DCM.xY.Dfile') 
% of an existing SPM for M/EEG-file with at least two evoked responses. 

clc; clear;

% Please replace filenames etc. by your own.
%--------------------------------------------------------------------------
spm('defaults','EEG');

% Data and analysis directories
%--------------------------------------------------------------------------

Pbase     = './spmeeg_data';        % directory with your data, 

Pdata     = fullfile(Pbase, '.'); % data directory in Pbase
Panalysis = fullfile(Pbase, '.'); % analysis directory in Pbase

cd(Panalysis)

subj_ids = {'h01','h02','h03','h04','h05','h06','h07',...
            'h08','h09','h10','h11','h12','h13','h14',...
            's01','s02','s03','s04','s05','s06','s07',...
            's08','s09','s10','s11','s12','s13','s14'};

for i = 1:length(subj_ids)
    
    subj_id = subj_ids(i);
    disp(subj_id)
% Data filename
%--------------------------------------------------------------------------
    %for j = 1
    for j = 1:30
    
        disp(strcat('trial no. ',int2str(j)))
% Parameters and options used for setting up model
%--------------------------------------------------------------------------
        DCM.xY.Dfile = strcat('spmeeg_',subj_id,'-auto-epo');
        DCM.options.analysis = 'CSD'; % analyze evoked responses
        DCM.options.model    = 'ERP'; % ERP model
        DCM.options.spatial  = 'IMG'; % spatial model
        DCM.options.trials   = j; % index of ERPs within ERP/ERF file
        DCM.options.Tdcm(1)  = 1;     % start of peri-stimulus time to be modelled
        DCM.options.Tdcm(2)  = 1000;   % end of peri-stimulus time to be modelled
        DCM.options.Fdcm(1) = 1;
        DCM.options.Fdcm(2) = 45;
        DCM.options.Nmodes   = 19;     % nr of modes for data selection
        DCM.options.h        = 5;     % nr of DCT components
        DCM.options.onset    = 64;    % selection of onset (prior mean)
        DCM.options.D        = 1;     % downsampling

%--------------------------------------------------------------------------
% Data and spatial model
%--------------------------------------------------------------------------
        DCM  = spm_dcm_erp_data(DCM);

%--------------------------------------------------------------------------
% Location priors for dipoles
%--------------------------------------------------------------------------
        DCM.Lpos  = [[-49; -66; 30] [49; -63; 33] [0; -58; 0] [-1; -54; 27]];
        DCM.Sname = {'lLP', 'rLP', 'Prec', 'mPFC'};
        Nareas    = size(DCM.Lpos,2);

%--------------------------------------------------------------------------
% Spatial model
%--------------------------------------------------------------------------
        DCM = spm_dcm_erp_dipfit(DCM);
        DCM = spm_dcm_csd_data(DCM);

%--------------------------------------------------------------------------
% Specify connectivity model
%--------------------------------------------------------------------------
        DCM.A{1} = zeros(Nareas,Nareas);
        DCM.A{1}(3,1) = 1;
        DCM.A{1}(4,1) = 1;
        DCM.A{1}(3,2) = 1;
        DCM.A{1}(4,2) = 1;
        DCM.A{1}(4,3) = 1;

        DCM.A{2} = zeros(Nareas,Nareas);
        DCM.A{2}(1,3) = 1;
        DCM.A{2}(2,3) = 1;
        DCM.A{2}(1,4) = 1;
        DCM.A{2}(2,4) = 1;
        DCM.A{2}(3,4) = 1;

        DCM.A{3} = zeros(Nareas,Nareas);
        DCM.A{3}(1,2) = 1;
        DCM.A{3}(2,1) = 1;

        %DCM.B{1} = DCM.A{1} + DCM.A{2};
        %DCM.B{1}(1,1) = 1;
        %DCM.B{1}(2,2) = 1;
        DCM.B{1} = zeros(Nareas, Nareas);

        DCM.C = [0; 0; 0; 0];

%--------------------------------------------------------------------------
% Between trial effects
%--------------------------------------------------------------------------
        %DCM.xU.X = [0 0 0 0; 1 0 0 0; 0 1 0 0; 0 0 1 0; 0 0 0 1];
        DCM.xU.X = 0;
        DCM.xU.name = {'standard'};

%--------------------------------------------------------------------------
% Invert
%--------------------------------------------------------------------------
        DCM.name = char(strcat('DCM_csd_',subj_id,'_',int2str(j)));

        %DCM      = spm_dcm_erp(DCM);
        DCM      = spm_dcm_csd(DCM);
    
        clear DCM;
    end
end

cd ..