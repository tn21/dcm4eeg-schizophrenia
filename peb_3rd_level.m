% Clean up
clc; clear;

% PEB settings
N = 14;
M   = struct();
M.Q = 'all';
M.X = ones(N,1);
M.maxit = 128;

% Load PEBs
PEBs_h = cell(N,1);
PEBs_s = cell(N,1);

subjects = cell(N,1);
for i = 1:N
    number = num2str(i,'%02d');
    subjects{i}    = strcat('h', number);
    path_h = strcat(pwd,'/PEB_2nd_results/', subjects{i} ,'/', subjects{i},'_PEB.mat');
    load(path_h,'PEB');
    PEBs_h{i} = PEB;
    subjects{i+14} = strcat('s', number);
    path_s = strcat(pwd,'/PEB_2nd_results/', subjects{i+14} ,'/', subjects{i+14},'_PEB.mat');
    load(path_s,'PEB');
    PEBs_s{i} = PEB;
end
%subjects = {("h01")};  % uncomment to calculate only one subject


%for i=1:N
%    path = strcat(pwd,'PEB_2nd_results/h0',num2str(i),'_results\PEB_h0'), num2str(i),'.mat');
%    load(path, 'PEB')
%    PEBs{i} = PEB;
%end
%for i=1:N/2-1
%    path = strcat('C:\Users\andre\Downloads\spm12\spm12\PEB_2nd_results\h01_results\PEB_s0', num2str(i),'.mat');
%    load(path, 'PEB')
%    PEBs{i} = PEB;
%end

% Save PEBs
%save(fullfile('PEB_3rd_results','2nd_PEBs_h.mat'), 'PEBs_h');
%save(fullfile('PEB_3rd_results','2nd_PEBs_s.mat'), 'PEBs_s');

% Compute PEB
PEB3_h = spm_dcm_peb(PEBs_h, M);
PEB3_s = spm_dcm_peb(PEBs_s, M);

save(fullfile('PEB_3rd_results','3rd_PEBs_h.mat'), 'PEB3_h');
save(fullfile('PEB_3rd_results','3rd_PEBs_s.mat'), 'PEB3_s');

% Bayesian model reduction
BMA3_h = spm_dcm_peb_bmc(PEB3_h);
%spm_dcm_peb_review(BMA3_h);
BMA3_s = spm_dcm_peb_bmc(PEB3_s);
%spm_dcm_peb_review(BMA3_s);

save(fullfile('PEB_3rd_results','3rd_BMA_h.mat'), 'BMA3_h');
save(fullfile('PEB_3rd_results','3rd_BMA_s.mat'), 'BMA3_s');
