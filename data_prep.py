"""EEG data preprocessing pipeline using mne-python.

For each subject do the following:
    1. Set positioning of sensors according to 10-20 system.
    2. Filter data
        - Remove power line (50 Hz, 100 Hz, ...)
        - Apply band pass filter (1 Hz to 45 Hz)
    3. Create fixed length windows (epochs) of 1 s size
        - Drop (bad) epochs with fluctuation above 100 mV
    4. [Optional:] Use ICA for artifact rejection
        - Either, use *automatic* mode which uses 'Fp1' and 'Fp2' channels to
            simulate an ECG source (need to pay attention!),
        - Or, inspect on your own and *hardcode* components to remove.
    5. Save (cleaned) epochs in "data_prep/epochs" directory.

"""


from pathlib import Path
from typing import Optional, Iterator

import numpy as np
import mne

import matplotlib
# import matplotlib.pyplot as plt

from utils._logging import Logging  # noqa
# from utils.mne_save_edf import write_mne_edf

logger = Logging().logger  # empty logger


# === DATA Preprocessing ===

def _filter_raw(raw: mne.io.BaseRaw, power_line=50, l_freq=1., h_freq=45.) -> mne.io.BaseRaw:
    logger.info("Filtering data...")
    nyquist = raw.info["sfreq"] / 2.
    
    # Remove power line
    logger.debug(f"Removing power line {power_line} Hz")
    raw.notch_filter(np.arange(power_line, nyquist, power_line), method="spectrum_fit")  # noqa
    # Apply bandpass
    logger.debug(f"Applying band pass filter from {l_freq} Hz to {h_freq} Hz")
    raw = raw.filter(l_freq=l_freq, h_freq=h_freq)
    
    return raw


def _make_epochs(raw: mne.io.BaseRaw, duration=1, overlap=0, reject_eeg=60e-6) -> mne.Epochs:
    logger.info("Making fixed length epochs...")
    
    epochs = mne.make_fixed_length_epochs(raw, duration=duration, overlap=overlap)
    
    # Drop too high fluctuations
    logger.debug(f"Drop (bad) epochs with fluctuations above {reject_eeg * 1e6} mV")
    reject = dict(eeg=reject_eeg)  # mV
    epochs.drop_bad(reject=reject, verbose=False)
    logger.debug(f"n_epochs = {len(epochs)}")
    
    return epochs


def _get_exclude_source_list(key: str) -> list:
    exclude = dict()
    exclude["h01"] = [8, 12]
    exclude["h02"] = [10, 12]
    exclude["h03"] = [5, 6, 12]
    exclude["h04"] = [1, 11, 12]
    exclude["h05"] = [8, 12]
    exclude["h06"] = [2, 7]
    exclude["h07"] = [5, 6, 7, 8]
    exclude["h08"] = [13]
    exclude["h09"] = [6]
    exclude["h10"] = [8, 9, 14]
    exclude["h11"] = [4, 5]
    exclude["h12"] = [None]
    exclude["h13"] = [None]
    exclude["h14"] = [None]
    exclude["s01"] = [None]
    exclude["s02"] = [None]
    exclude["s03"] = [None]
    exclude["s04"] = [None]
    exclude["s05"] = [None]
    exclude["s06"] = [None]
    exclude["s07"] = [None]
    exclude["s08"] = [None]
    exclude["s09"] = [None]
    exclude["s10"] = [None]
    exclude["s11"] = [None]
    exclude["s12"] = [None]
    exclude["s13"] = [None]
    exclude["s14"] = [None]
    return exclude[key]


def _ica_reject_artifacts(raw: mne.io.BaseRaw, epochs, mode: str,
                          inf_dir: Path = None, subj="", **kws) -> Optional[mne.io.BaseRaw]:
    logger.info(f"Using ICA for artifact rejection with {mode} mode...")
    
    logger.debug("Fitting ICA to epochs data...")
    ica = mne.preprocessing.ICA(**kws)
    ica.fit(epochs)
    
    if "INSPECT" in mode:
        components = ica.plot_components(inst=raw, show=False)
        sources = ica.plot_sources(raw, stop=30, show=False, show_scrollbars=False)
        
        logger.debug(f"Writing images to {inf_dir}/...")
        components[0].savefig(str(inf_dir / subj) + "-components.jpeg")
        sources.savefig(str(inf_dir / subj) + "-sources.jpeg")
        
        return None
    
    if "APPLY" in mode:
        if "HARD" in mode:
            ica.exclude = _get_exclude_source_list(subj)
        else:  # "AUTO"
            # Create a bipolar reference from frontal EEG sensors and use that as virtual EOG channel.
            # ATTENTION: You must hope that the frontal EEG channels only reflect EOG
            #            and not brain dynamics in the prefrontal cortex
            name_EOG = "EOG_simulated"
            sim_eog = mne.set_bipolar_reference(raw, "Fp1", "Fp2", name_EOG).pick_channels([name_EOG])
            raw_w_sim_eog = raw.copy().add_channels([sim_eog])
            # find which ICs match the (simulated) EOG pattern
            eog_indices, eog_scores = ica.find_bads_eog(raw_w_sim_eog, name_EOG)
            ica.exclude = eog_indices
        
        logger.debug(f"ica.exclude = {ica.exclude}  ({mode[-4:]})")
        
        raw_ica = raw.copy()
        ica.apply(raw_ica)
        
        return raw_ica
    
    raise NotImplementedError("Neither INSPECT nor APPLY mode activated.")


def _handle_subject(subj_id: str, src_path: Path, inf_dir: Path, artifact_mode="INSPECT"):
    # Handle input variable
    artifact_mode = str(artifact_mode).upper()
    assert artifact_mode in ("INSPECT", "APPLY", "APPLY_AUTO", "APPLY_HARD")
    logger.warning(f"Handling subject {subj_id} in {artifact_mode} artifact mode")
    
    # Read data
    logger.info("Reading raw file...")
    raw = mne.io.read_raw_edf(str(src_path))
    logger.debug("Setting standard 10-20 montage")
    raw.set_montage("standard_1020")
    raw._raw_extras[0]['subject_info']['name'] = str(subj_id)  # noqa
    raw_orig = raw.copy()
    
    # --- Basic cleanup ---
    raw.load_data()
    # Filter
    raw = _filter_raw(raw, power_line=50, l_freq=1., h_freq=45.)
    # A common compromise for Low Density systems is referencing to the Fpz channel.
    # http://journal.frontiersin.org/article/10.3389/fnins.2017.00109/full
    raw.set_eeg_reference(ref_channels=["Fp1", "Fp2"], projection=False)
    
    # Make fixed length epochs
    epochs = _make_epochs(raw, duration=1, overlap=0, reject_eeg=100e-6)
    
    # ICA artifact rejection
    ica_kws = dict(n_components=15, max_iter='auto', random_state=97)
    inf_ica_dir = inf_dir / "ICA"
    inf_ica_dir.mkdir(exist_ok=True)
    raw = _ica_reject_artifacts(raw, epochs, artifact_mode, inf_dir=inf_ica_dir, subj=subj_id, **ica_kws)
    
    # Re-calculate epochs and save
    if isinstance(raw, mne.io.base.BaseRaw):
        # Again, make fixed length epochs
        epochs = _make_epochs(raw, duration=1, overlap=0, reject_eeg=100e-6)
        # Define file path and save as *.fif
        inf_epo_dir = inf_dir / "epochs"
        inf_epo_dir.mkdir(exist_ok=True)
        fname = str(inf_epo_dir / f"{subj_id}-{artifact_mode[-4:].lower()}-epo.fif")
        logger.info(f"Saving epochs in {fname}...")
        epochs.save(fname, overwrite=True)
    else:
        logger.info("Did not save raw file - use `mode='APPLY'` otherwise")
    
    del epochs, raw, raw_orig
    return


# === USER INTERFACE ===

def get_data_iterator(src_dir="data_prep/epochs", select: str = None) -> Iterator[mne.Epochs]:
    """Iterate over the preprocessed data.
    
    Args:
        src_dir: Source directory of the '*.fif' files.
        select: Select file name pattern/regex(?) inside `src_dir`.

    Yields:
        epochs (mne.Epochs): Epoch data.
    
    Examples:
        >>> for epoch_data in get_data_iterator(select="*-auto-epo.fif"):
        >>>     print(epoch_data.info['meas_date'])  # do stuff
    
    """
    
    if select is None:
        select = "*.fif"
    
    for fname in sorted(Path(src_dir).glob(select)):
        epochs: mne.Epochs = mne.read_epochs(fname)
        yield epochs


# === MAIN ===

def main(artifact_mode: str = "APPLY_AUTO"):
    
    src_dir = Path("EEG_dataset_V1")
    inf_dir = Path("data_prep")
    inf_dir.mkdir(exist_ok=True)
    
    subject_paths = sorted(src_dir.glob("*.edf"))  # [... '<path-to-edf-file>' ...]
    
    for subj_path in subject_paths:
        subj_id = subj_path.stem  # e.g. 'h01'
        _handle_subject(subj_id, subj_path, inf_dir, artifact_mode=artifact_mode)
    
    return


if __name__ == '__main__':
    mne.set_log_level(verbose=False)
    # Add console logger
    logging = Logging(__name__, "DEBUG").add_ch().add_fh(f"data_prep/{Path(__file__).stem}")
    logger = logging.logger
    # Change matplotlib mode for interactive plotting
    logger.info("Using `%matplotlib qt`")
    matplotlib.use('Qt5Agg')  # Use PyQt5
    # Main
    main()
