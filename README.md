# dcm4eeg - Schizophrenia

## Dataset: EEG in schizophrenia

The dataset comprised 14 patients with paranoid schizophrenia and 14 healthy controls. Data were acquired with the sampling frequency of 250 Hz using the standard 10-20 EEG montage with 19 EEG channels: Fp1, Fp2, F7, F3, Fz, F4, F8, T3, C3, Cz, C4, T4, T5, P3, Pz, P4, T6, O1, O2. The reference electrode was placed between electrodes Fz and Cz.

[Link to dataset](https://repod.icm.edu.pl/dataset.xhtml?persistentId=doi:10.18150/repod.0107441)

## Preprocessing

Run ```data_prep.py``` for preprocessing.

## DCM for CSD

Some files in spm12 are modified to fit for our data. The modified versions are listed in ```\spm_files```. They should replace the original ones for the next steps.

Run ```dcm_all_windows.m``` to calculate for DCM for CSD for each window of all subjects. (Attention: This may take a while.) Make sure that for each participant the EEG data is inside the `spmeeg_data` folder and is named accordingly, e.g. for the subject S01 `spmeeg_h01-auto-epo.dat`.

The DCM data should be moved from the `spmeeg_data` folder into `DCM_csd_data`. After that, `cd` into that directory and run the bash script (`bash arrange_dcm_data.sh`). The data should now be ready for further processing.

For the most up-to-date DCMs, check out this link: https://polybox.ethz.ch/index.php/s/cL8GAXTf7cQjHWJ.

## 2nd PEB: window level to subject level

Run ```peb_design_matrix.m``` to generate the design matrix.

Run `peb_2nd_level.m`. The resulting data will be stored in a folder called `PEB_2nd_results`.

## 3rd PEB: subject level to group level

Run `peb_3rd_level.m`. The resulting data will be stored in a folder called `PEB_3rd_results`.
