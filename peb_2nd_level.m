% Clean up
clc; clear;

% Define paths
src_dir = fullfile(pwd, 'DCM_csd_data');  % source data
inf_dir = fullfile(pwd, 'PEB_2nd_results');      % inferred data
if ~exist(inf_dir, 'dir'), mkdir(inf_dir); end


% Init
save_example = true;
subjects = cell(14,1);
for i = 1:14
    number = num2str(i,'%02d');
    subjects{i}    = strcat('h', number);
    subjects{i+14} = strcat('s', number);
end
%subjects = {("h01")};  % uncomment to calculate only one subject

% Iterate every subject
for idx = 1:size(subjects,1)
    
    subj = string(subjects{idx});  % subject identifier

    % Define subject specific paths
    subj_src_dir = fullfile(src_dir, subj);   % subject source data
    subj_inf_dir = fullfile(inf_dir, subj);   % subject inferred data
    if ~exist(subj_inf_dir, 'dir'), mkdir(subj_inf_dir); end

    %--------------------------------------------------------------------------

    % Create GCM file, given the DCM data
    GCM = dcm_gcm_from_dir(subj_src_dir);

    % Write results
    if save_example
        save(fullfile(inf_dir,'GCM_example.mat'), 'GCM');
    end
    save(fullfile(subj_inf_dir,strcat(subj,'_GCM.mat')), 'GCM');

    %--------------------------------------------------------------------------

    % load design matrix
    dm = load('design_matrix.mat');

    % PEB settings
    M   = struct();
    M.Q = 'all'; 
    % M.X = ones(size(GCM));
    M.X      = dm.X;
    M.Xnames = dm.labels;

    % Estimate model
    field = {'A'};  % choose field    
    %PEB = spm_dcm_peb(GCM, M, field);
    [BMC, PEB] = spm_dcm_bmc_peb(GCM, M, field);

    % Write results
    if save_example
        save(fullfile(inf_dir,'PEB_example.mat'), 'PEB');
    end
    save(fullfile(subj_inf_dir,strcat(subj,'_PEB.mat')), 'PEB');
    save(fullfile(subj_inf_dir,strcat(subj,'_BMC.mat')), 'PEB');

    %--------------------------------------------------------------------------

    % Specify candidate models that differ in particular A-matrix connections.
    % GCM file with each of your connectivity hypotheses.
    %GCM = specify_canidate_models(GCM);

    % Compare nested PEB models. Decide which connections to switch off based
    % on the structure of each template DCM in the GCM cell array.
    %BMA = spm_dcm_peb_bmc(PEB, GCM);
    %spm_dcm_peb_review(BMA, GCM);

    % Write results
    %if save_example
    %    save(fullfile(inf_dir,'GCM_templates.mat'), 'GCM');
    %    save(fullfile(inf_dir,'BMA_example.mat'), 'BMA');
    %end
    %save(fullfile(subj_inf_dir,strcat(subj,'_GCM_templates.mat')), 'GCM');
    %save(fullfile(subj_inf_dir,strcat(subj,'_BMA.mat')), 'BMA');
    
end

%==========================================================================

function GCM = dcm_gcm_from_dir(subj_dir)
    % Find all DCM files
    regex = '[\d]\.mat$';
    dcms = spm_select('FPListRec', subj_dir, regex);
    
    % Character array -> cell array
    GCM = cellstr(dcms);
    
    % Filenames -> DCM structures
    GCM = spm_dcm_load(GCM);
    
    % DCM estimation already done
%     % Estimate DCMs (this won't effect original DCM files)
%     GCM = spm_dcm_fit(GCM);
end

function GCM = specify_canidate_models(input_GCM)
    % Specify candidate models that differ in particular A-matrix connections
    % % --- e.g. ---
    % DCM.A{1}(2,3) = 0;  % switching off the connection from region 3 to
    % region 2
    % 
    % % --- e.g. ---
    % DCM_model1 = DCM_full;
    % DCM_model1.A{1}(3,1) = 0;
    % DCM_model1.A{1}(1,3) = 0;
    % 
    % GCM = {DCM_full, DCM_model1};
    
    % Get an existing model. We'll use the first subject's DCM as a template
    DCM_full = input_GCM{1};

    % IMPORTANT: If the model has already been estimated, clear out the old
    %            priors, or changes to DCM.a,b,c will be ignored
    if isfield(DCM_full, 'M')
        DCM_full = rmfield(DCM_full, 'M');
    end

    % Original, full GCM model
    GCM_0 = {DCM_full};

    % Modifications setting only one connection to zero
    connections = {[3,1]; [3,2]; [4,1]; [4,2]; [4,3]};
    GCM_1 = cell(1, size(connections,1));
    n = 1;
    for i = 1:size(connections,1)
        DCM_variant = DCM_full;
        index = connections{i};
        DCM_variant.A{1}(index)       = 0;
        DCM_variant.A{2}(flip(index)) = 0;
        GCM_1{n} = DCM_variant;
        n = n + 1;
    end

    % Modifications setting two connections to zero
    combinations = nchoosek(connections,2);
    GCM_2 = cell(1, size(combinations,1));
    n = 1;
    for i = 1:size(combinations,1)
        DCM_variant = DCM_full;
        for j = 1:size(combinations,2)
            index = combinations{i,j};
            DCM_variant.A{1}(index)       = 0;
            DCM_variant.A{2}(flip(index)) = 0;
        end
        GCM_2{n} = DCM_variant;
        n = n + 1;
    end

    % Combine
    GCM = [GCM_0, GCM_1, GCM_2];
end

%--------------------------------------------------------------------------
