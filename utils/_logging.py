import logging
from typing import Union


class Logging:
    def __init__(self, name: str = None, level: Union[int, str] = "NOTSET", fmt: str = None):
        if name is None:
            name = __name__
        self.logger = logging.getLogger(name)
        self.logger.setLevel(level)
        
        self.sh_formatter = CustomFormatter(fmt)
        self.fh_formatter = CustomFormatter(fmt, with_colors=False)
    
    def add_file_handler(self, filename: str = None, level: Union[int, str] = "NOTSET"):
        if filename is None:
            filename = self.logger.name
        
        fh = logging.FileHandler(f"{filename}.log", mode="w")
        fh.setLevel(level)
        fh.setFormatter(self.fh_formatter)
        
        self.logger.addHandler(fh)
        return self
    
    def add_stream_handler(self, level: Union[int, str] = "NOTSET"):
        ch = logging.StreamHandler()
        ch.setLevel(level)
        ch.setFormatter(self.sh_formatter)
        
        self.logger.addHandler(ch)
        return self

    def add_fh(self, *args, **kwargs):
        return self.add_file_handler(*args, **kwargs)
    
    def add_ch(self, *args, **kwargs):
        return self.add_stream_handler(*args, **kwargs)
    
    # @staticmethod
    # def _check_level(level):
    #     levels = {"notset": logging.NOTSET, "debug": logging.DEBUG, "info": logging.INFO,
    #               "warning": logging.WARNING, "error": logging.ERROR, "critical": logging.CRITICAL}
    #     if level is None:
    #         return logging.NOTSET
    #     elif level in levels.keys():
    #         return levels[level]
    #     elif level in levels.values():
    #         return level
    #     else:
    #         raise ValueError(f"Invalid value. Should be one of {tuple(levels)}.")


class CustomFormatter(logging.Formatter):
    """Logging Formatter to add colors and count warning / errors"""

    grey = "\x1b[38;2;170;170;170m"
    green = "\x1B[38;2;64;180;64m"
    yellow = "\x1B[38;2;255;170;0m"
    red = "\x1B[38;2;255;40;0m"
    bold = "\x1B[1m"
    reset = "\x1b[0m"
    
    fmt_old = "%(levelname)-8s: %(name)-20s %(message)-40s  (%(filename)s:%(lineno)d)"
    fmt = "%(message)-60s (%(filename)s:%(lineno)d)"
    
    FORMATS = {
        logging.DEBUG:    grey + fmt + reset,
        logging.INFO:     green + fmt + reset,
        logging.WARNING:  yellow + fmt + reset,
        logging.ERROR:    red + fmt + reset,
        logging.CRITICAL: bold + red + fmt + reset
    }
    FORMATS_NO_COLORS = {
        logging.DEBUG:    "D : " + fmt,
        logging.INFO:     "I : " + fmt,
        logging.WARNING:  "W : " + fmt,
        logging.ERROR:    "E : " + fmt,
        logging.CRITICAL: "C : " + fmt
    }
    
    def __init__(self, *args, fmt: str = None, with_colors=True, **kwargs):
        super().__init__(*args, **kwargs)
        if fmt is not None:
            self.fmt = fmt
        self.with_colors = with_colors
    
    def format(self, record):
        if self.with_colors:
            log_fmt = self.FORMATS.get(record.levelno)
        else:
            log_fmt = self.FORMATS_NO_COLORS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)
