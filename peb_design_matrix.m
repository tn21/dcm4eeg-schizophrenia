% Clean up
clc; clear;

% Init
W = 30;  % num rows / windows
P = 5;  % num cols / parameters
time = (0:W-1).';

% Define columns
const = ones(W, 1);  % constant term
decay = exp(-time/20);  % exponential decay
dct = spm_dctmtx(W, 5);  % discrete cosine transformation
dct = dct(:,3:end);

% Concat and store
X = cat(2, const, decay, dct);
labels = ["const", "exp_decay", "DCT1", "DCT2", "DCT3"];
save("design_matrix.mat", "X");
save("design_matrix.mat", "labels", "-append");

% Clean up
clear const decay dct
